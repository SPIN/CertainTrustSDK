/**
 * CertainTrust Demonstrator in Java
 * 
 * This class allows the applet to run as Java application.
 * 
 * @author	Florian Volk <florian.volk@cased.de>
 */

public class DemonstratorStarter {

	public static void main(String[] args) {
		// create the applet
		DemonstratorCD demonstrator = new DemonstratorCD();
		demonstrator.init();
		demonstrator.start();
		
		// host the applet in a swing window
		javax.swing.JFrame window = new javax.swing.JFrame("CertainLogic Demonstrator");
		window.setContentPane(demonstrator);
		window.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);
	}

}
