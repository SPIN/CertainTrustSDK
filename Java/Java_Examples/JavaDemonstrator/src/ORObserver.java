/**
 * CertainTrust Demonstrator in Java
 * 
 * The ORObserver is used to update the result of the OR operation
 * when the operands change and display the result in the result HTI.
 * 
 * @author	Florian Volk <florian.volk@cased.de>
 */

import java.util.Observable;
import java.util.Observer;

import CertainTrust.CertainTrust;


public class ORObserver implements Observer {

	private CertainTrust Operand1;
	private CertainTrust Operand2;
	private CertainTrust Result;
	
	public ORObserver(CertainTrust Operand1, CertainTrust Operand2, CertainTrust Result) {
		this.Operand1 = Operand1;
		this.Operand2 = Operand2;
		this.Result = Result;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// calculate the result of the AND operation and update the Result CertainTrust data object
		CertainTrust ORResult = this.Operand1.OR(this.Operand2);
		this.Result.setF(ORResult.getF());
		this.Result.setTC(ORResult.getT(), ORResult.getC());
	}
}
