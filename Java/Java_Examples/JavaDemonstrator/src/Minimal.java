/**
 * Minimal CertainTrust Demonstrator in Java
 * 
 * Demonstrates the basic usage of the CertainTrust SDK.
 * 
 * @author	Florian Volk <florian.volk@cased.de>
 */

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import CertainTrust.CertainTrust;
import CertainTrust.CertainTrustHTI;


public class Minimal extends JFrame implements ActionListener {
	private static final long serialVersionUID = -447167281994322634L;

	// this object stores the trust data and implements the operators
	CertainTrust ctObject;
	
	public Minimal() {
		setTitle("Minimal CertainTrust SDK Demonstrator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		
		
		// display a single HTI
		ctObject = new CertainTrust(10); // first, we need a CertainTrust data object
		add(new CertainTrustHTI(ctObject));
		
		// add a button to read the CertainTrust values
		JButton button = new JButton("Read CertainTrust values");
		button.addActionListener(this);
		add(button);
		
		
		this.setSize(450, 250);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// whenever the button is clicked
		JOptionPane.showMessageDialog(this,
				"Values of the CertainTrust object:\n"
				+ "\nInit. value: "	+ this.ctObject.getF()
				+ "\nTrust: "		+ this.ctObject.getT()
				+ "\nCertainty: "	+ this.ctObject.getC()
				+ "\nExpectation: "	+ this.ctObject.getExpectation());
	}
	
	
	public static void main(String[] args) {
		new Minimal();
	}
}
