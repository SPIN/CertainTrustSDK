import java.util.ArrayList;
import java.util.List;

import CertainTrust.CertainTrust;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.Lighting;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class CertainTrustTViz extends Application {
	
	public static void main(String[] args) {
        Application.launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("CertainTrust TViz for JavaFx");

        //Lighting glow = new Lighting();
        
        Group root = new Group(); // Creating layout
        //root.setEffect(glow);
        
        Scene scene = new Scene(root, 550, 550);
        
        Circle circle1 = new Circle();
        circle1.setCenterX(275.0f);
        circle1.setCenterY(275.0f);
        circle1.setRadius(65.0f);
        circle1.setFill(Color.TRANSPARENT);
        circle1.setStroke(Color.BLACK);
        circle1.setStrokeWidth(0.15);
        
        Circle circle2 = new Circle();
        circle2.setCenterX(275.0f);
        circle2.setCenterY(275.0f);
        circle2.setRadius(105.0f);
        circle2.setFill(Color.TRANSPARENT);
        circle2.setStroke(Color.BLACK);
        circle2.setStrokeWidth(0.15);
        
        Circle circle3 = new Circle();
        circle3.setCenterX(275.0f);
        circle3.setCenterY(275.0f);
        circle3.setRadius(145.0f);
        circle3.setFill(Color.TRANSPARENT);
        circle3.setStroke(Color.BLACK);
        circle3.setStrokeWidth(0.15);
        
        Circle circle4 = new Circle();
        circle4.setCenterX(275.0f);
        circle4.setCenterY(275.0f);
        circle4.setRadius(185.0f);
        circle4.setFill(Color.TRANSPARENT);
        circle4.setStroke(Color.BLACK);
        circle4.setStrokeWidth(0.15);
        
        List<CertainTrust> ctObject = new ArrayList<>();
        CertainTrust a = new CertainTrust(0.9,1,0.9,100);
        a.setName("Cloud A");
        CertainTrust b = new CertainTrust(0.75,1,0.9,100);
        b.setName("Cloud B");
        CertainTrust c = new CertainTrust(0.5,1,0.9,100);
        c.setName("Cloud C");
        CertainTrust d = new CertainTrust(0.25,1,0.9,100);
        d.setName("Cloud D");
        CertainTrust e = new CertainTrust(0.15,1,0.9,100);
        e.setName("Cloud E");
        CertainTrust f = new CertainTrust(0.35,1,0.9,100);
        f.setName("Cloud F");
        ctObject.add(a);
        ctObject.add(b);
        ctObject.add(c);
        ctObject.add(d);
        ctObject.add(e);
        //ctObject.add(f);
        int size = ctObject.size();
        float arcSize = 360/size;
                
        float avg=0;
        
        for(int i = 0; i < size; i++) {
        	avg = (float) (avg + ctObject.get(i).getT());
        }
        
        int temp = (int)(avg / size *100);
        String midData = String.valueOf(temp);
        midData = midData + "%";
        
        Circle middle = new Circle();
        middle.setCenterX(275.0f);
        middle.setCenterY(275.0f);
        middle.setRadius(25.0f);
        middle.setFill(Color.WHITE);
        middle.setStroke(Color.BLACK);
        
        Label   label   = new Label(midData);
        label.setTranslateX(265);
        label.setTranslateY(265);
        
        List<Node> list = new ArrayList<>();

        root.getChildren().add(circle1);
        root.getChildren().add(circle2);
        root.getChildren().add(circle3);
        root.getChildren().add(circle4);
        
        // Create arcs dynamically
        for(int i = 0; i < size; i++) {
        	
        	// color calculation for arcs
        	double resultp2 = ((1-ctObject.get(i).getC())*ctObject.get(i).getF());
        	double result = (ctObject.get(i).getT()*ctObject.get(i).getC()) + resultp2;
        	
        	int R = 0;
        	int G = 0;
        	int B = 0;
        	
        	if (result < 0.5) {
        		R = 255;
        		G =	(int) Math.min(255, (255 * 2 * result));
        	} else {
        		R =	(int) Math.min(255, ((2 - (2 * result)) * 255));
        		G =	255;
        	}
        	
        	Arc newArc = new Arc();
        	newArc.setCenterX(275.0f);
        	newArc.setCenterY(275.0f);
        	newArc.setRadiusX(ctObject.get(i).getT()*225);
        	newArc.setRadiusY(ctObject.get(i).getT()*225);
        	newArc.setStartAngle(0.0+i*arcSize);
        	newArc.setLength(arcSize);
        	newArc.setType(ArcType.ROUND);
        	newArc.setFill(Color.rgb(R,G,B));
        	
        	Arc arcBorder = new Arc();
        	arcBorder.setCenterX(275.0f);
        	arcBorder.setCenterY(275.0f);
        	arcBorder.setRadiusX(225.0f);
        	arcBorder.setRadiusY(225.0f);
        	arcBorder.setStartAngle(i*arcSize);
        	arcBorder.setLength(arcSize);
        	arcBorder.setType(ArcType.ROUND);
        	arcBorder.setFill(Color.TRANSPARENT);
        	arcBorder.setStroke(Color.BLACK);
        	
        	int labelx;
        	int labely;
        	float angle = ((i * arcSize) + (arcSize/2)) * (-1);
        	
        	labelx = (int) (225*(Math.cos(Math.toRadians(angle))) + 275);
        	labely = (int)(225*(Math.sin(Math.toRadians(angle))) + 275);
        	
        	Label   arcLabel   = new Label(ctObject.get(i).getName());
            arcLabel.setTranslateX(labelx);
            arcLabel.setTranslateY(labely);
            
        	list.add(newArc);
        	list.add(arcBorder);
        	list.add(arcLabel);
        }
        
       	for(int i = 0; i < size*3; i++) {
       		root.getChildren().add(list.get(i));
       	}
       	
       	root.getChildren().add(middle);
       	root.getChildren().add(label);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}